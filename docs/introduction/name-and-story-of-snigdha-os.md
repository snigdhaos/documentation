---
sidebar_position: 2
---
# Story & Name

### 🌟 **The Story and Name Behind Snigdha OS**

#### 💫 **Project Evolution**
Snigdha OS began its journey as **"Project N"**, a codename that held special significance during its early development phase. The project was initially conceived by **Eshan Roy** (eshanized) in early 2023 as an innovative approach to creating a lightweight, security-focused Linux distribution.

#### 💝 **The Name's Origin**
The name **"Snigdha"** carries profound meaning and emotional significance:

- **Etymology**: "Snigdha" (স্নিগ্ধ) in Bengali means "affectionate" or "loving"
- **Inspiration**: Named after **Mubasshira Snigdha**, Eshan Roy's late girlfriend
- **Legacy**: Represents both a personal tribute and a commitment to creating something meaningful
- **Official Adoption**: Formally named on December 31, 2023, marking its first public release

#### 👨‍💻 **The Creator**
**Eshan Roy** brings significant experience to the project:
- Founder and Lead Maintainer of Snigdha OS
- Former CEO of Tonmoy Infrastructure
- Passionate advocate for open-source development
- Dedicated to cybersecurity and system optimization

#### 💔 **A Personal Journey**
The story behind Snigdha OS is deeply personal and touching:

- **Tragic Loss**: Mubasshira Snigdha passed away on June 18, 2023
- **Transformation**: Eshan channeled his grief into creating something meaningful
- **Memorial**: The project serves as a living tribute to her memory
- **Purpose**: Combines personal meaning with technological innovation

#### 🤝 **Corporate Support**
**Tonmoy Infrastructure** continues to play a vital role:
- Provides infrastructure and resources
- Supports ongoing development
- Ensures project sustainability
- Maintains professional backing

#### 🌱 **Growing Legacy**
Snigdha OS represents more than just technology:
- Demonstrates how personal loss can inspire creation
- Shows the power of open-source communities
- Combines professional excellence with personal meaning
- Creates lasting impact through innovation

#### 🎯 **Future Vision**
The project continues to grow while honoring its namesake:
- Expanding features and capabilities
- Building a stronger community
- Maintaining personal connection
- Advancing technological boundaries

#### 💫 **Impact and Inspiration**
Snigdha OS serves as:
- A reminder of love's lasting influence
- An inspiration for turning grief into creation
- A platform for technological innovation
- A community-driven success story

### ✨ **Conclusion**
Snigdha OS embodies the beautiful intersection of personal meaning and technological innovation. It stands as a testament to how profound loss can inspire the creation of something meaningful and valuable for the global community.

Through this project, Mubasshira Snigdha's memory lives on, touching lives worldwide through technology that helps and empowers others. The project continues to grow and evolve, guided by the principles of love, dedication, and excellence that inspired its creation. ❤️🌟