---
sidebar_position: 1
---

# Common Issues

### 🔍 **Common Issues and Solutions**

Quick solutions for frequently encountered problems in Snigdha OS.

### 🖥️ **Boot Issues**
1. **GRUB Not Loading**
   - Check BIOS settings
   - Repair GRUB using live USB
   - Verify boot partition

2. **Kernel Panic**
   - Boot with previous kernel
   - Check hardware compatibility
   - Verify kernel parameters

### 🌐 **Network Issues**
1. **No Internet Connection**
   ```bash
   # Check network status
   nmcli device status

   # Restart network manager
   sudo systemctl restart NetworkManager
   ```

2. **Wi-Fi Problems**
   - Update wireless drivers
   - Check hardware switches
   - Verify network configuration

### 🔐 **System Access**
1. **Password Recovery**
   - Use recovery mode
   - Reset root password
   - Update user credentials

2. **Permission Issues**
   - Check file ownership
   - Verify group permissions
   - Update ACLs if needed