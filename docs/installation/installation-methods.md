---
sidebar_position: 2
---

# Installation Methods

### 🔧 **Installation Methods for Snigdha OS**

Snigdha OS can be installed using several methods. Choose the one that best suits your needs:

### 💿 **USB Installation**
The most common method for installing Snigdha OS:

1. **Download the ISO**
   - Visit the official Snigdha OS website
   - Download the latest ISO file

2. **Create Bootable USB**
   - Use tools like Rufus, Etcher, or dd
   - Minimum 4GB USB drive required

3. **Boot from USB**
   - Enter BIOS/UEFI settings
   - Change boot order to prioritize USB
   - Save and restart

### 🖥️ **Virtual Machine Installation**
Perfect for testing or development environments:

1. **Supported Platforms**
   - VirtualBox
   - VMware
   - QEMU/KVM

2. **Requirements**
   - Enable virtualization in BIOS
   - Allocate minimum resources:
     - 2GB RAM
     - 20GB storage
     - 2 CPU cores

### ☁️ **Cloud Installation**
For cloud-based deployments:

1. **Supported Providers**
   - AWS
   - DigitalOcean
   - Google Cloud
   - Azure

2. **Requirements**
   - Valid cloud provider account
   - Basic knowledge of cloud infrastructure